import React, { Component } from "react";
import Item1 from "./ItemComponent/Item1";

export default class Item extends Component {
  render() {
    return (
      <>
        <div className="itemSection">
          <div className="container px-lg-5 pt-4">
            <div className="row gx-lg-5">
              <><Item1/></>
              <><Item1/></>
              <><Item1/></>
              <><Item1/></>
              <><Item1/></>
              <><Item1/></>
            </div>
          </div>
        </div>
      </>
    );
  }
}
