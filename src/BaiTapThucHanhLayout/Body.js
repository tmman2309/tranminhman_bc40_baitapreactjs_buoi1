import React, { Component } from "react";
import Banner from "./Banner";
import Footer from "./Footer";
import Header from "./Header";
import Item from "./Item";

export default class body extends Component {
  render() {
    return (
      <>
        <section id="body">
          <Header />
          <Banner />
          <Item />
          <Footer />
        </section>
      </>
    );
  }
}
