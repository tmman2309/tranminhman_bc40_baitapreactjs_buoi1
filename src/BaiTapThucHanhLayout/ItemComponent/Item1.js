import React, { Component } from "react";
import style from "../ex1.module.css";

export default class Item1 extends Component {
  render() {
    return (
      <>
        <div className="col-lg-6 col-xxl-4 mb-5">
          <div className="card bg-light border-0 h-100">
            <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
              <div className={style.itemCSS}>
                <i class="fa fa-cloud-download-alt"></i>
              </div>
              <h2 className="fs-4 fw-bold">Free to download</h2>
              <p className="mb-0">
                As always, Start Bootstrap has a powerful collectin of free
                templates.
              </p>
            </div>
          </div>
        </div>
      </>
    );
  }
}
